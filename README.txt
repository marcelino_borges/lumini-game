DESCRIPTION:
Lumini was a game created to a game jam made in 2018, in the city of Recife, Pernambuco, Brazil, 
organized by Senac/PE and IGDA/Recife. Lumini is a rare fish, who has lost his light. 
The only way to get it back permanently is finding a special pearl that will restore his luminescence.
While Lumini faces evil enemies in the ocean, guardians of the pearl obstinated to keep him away from his final 
and, not less, vital goal, he eats luminescent seaweeds to get a temporary light on his way to the pearl's hidden cave.


TEAM:
Marcelino Borges (Git Owner) - Programmer and 3D Artist
Caio Filizola - Programmer and 3D Artist
Flavio Mattos - 2D Artist
Giulia Sotero - 2D Artist
Jessica Bastos - Game Designer
Danilo Lopes - Game Designer
Vinicius Virtuoso - Music Artist


YouTube Clip:
https://youtu.be/pc_BJ4_2fy0


GameJolt - Available to play:
https://gamejolt.com/games/Lumini/315665


Minimum requirements: 
intel core i5, 536MB in hard disk, 2GB 128bits graphic card, 8GB RAM.

Art:
https://www.artstation.com/artwork/VxeZ8


